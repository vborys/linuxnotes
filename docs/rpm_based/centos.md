# CentOS afterinstall

!!! quote "About"
    CentOS is a Linux distribution that provides a free and open-source community-supported computing platform, functionally compatible with its upstream source, Red Hat Enterprise Linux (RHEL).

     Source: [https://centos.org/](https://centos.org/)


## Update
```
sudo yum update
```

---

## Install EPEL-repo and soft
```
sudo yum install epel-release
sudo yum repolist
sudo yum install dnf net-tools bash-completion htop wget nano 
```

---

## Change hostame
```
sudo hostnamectl set-hostname host.example.com
sudo hostnamectl set-hostname "Your Pretty HostName" --pretty
#sudo hostnamectl set-hostname host.example.com --static
#sudo hostnamectl set-hostname host.example.com --transient
sudo systemctl restart systemd-hostnamed
```

---

## Network config
```
sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3
```
>
TYPE="Ethernet" <br>
PROXY_METHOD="none" <br>
BROWSER_ONLY="no" <br>
BOOTPROTO="static" <br>
DEFROUTE="yes" <br>
IPV4_FAILURE_FATAL="no" <br>
IPV6INIT="yes" <br>
IPV6_AUTOCONF="yes" <br>
IPV6_DEFROUTE="yes" <br>
IPV6_FAILURE_FATAL="no" <br>
IPV6_ADDR_GEN_MODE="stable-privacy" <br>
NAME="eth0" <br>
UUID="bc480a15-d3a9-4650-8ed6-f8ff9259f646" <br>
DEVICE="eth0" <br>
ONBOOT="yes" <br>
IPADDR=192.168.122.100 <br>
NETMASK=255.255.255.0 <br>
GATEWAY=192.168.122.1 <br>
DNS1=8.8.8.8

```
sudo systemctl restart NetworkManager
sudo systemctl restart network
```

---

## SSH
```
sudo dnf install openssh-server openssh-clients
sudo systemctl enable --now sshd
sudo systemctl status sshd
```




