## SYSTEMD
List of services, targets, etc..

/usr/lib/systemd/system/...
```
systemctl
systemctl status httpd
systemctl enable {disable} httpd
systemctl start {stop, restart} httpd
 

systemctl list-unit-files --type=service {target, socket}

```

---
 
## SELINUX
>
[https://wiki.archlinux.org/title/SELinux](https://wiki.archlinux.org/title/SELinux) <br>
[https://youtu.be/fibllWD1_4s](https://wiki.archlinux.org/title/SELinux)

```
/etc/selinux/config
SELINUX=disabled {enforcing, permissive}

sestatus/getenforce

sudo setenforce 0 {1}

semanage fcontext -a -t httpd_sys_content_t "/web(/.*)?"
restorecon -R -v /web




sudo semanage port -a -t ssh_port_t -p tcp 2233
```

---

## JOURNALCTL
```
sudo journalctl -eu sshd

grep SOMETHING /var/log/messages
```

---

## FIREWALL
```
sudo systemctl enable firewalld
sudo systemctl start firewalld
sudo firewall-cmd --state
running


firewall-cmd --get-default-zone
firewall-cmd --get-active-zones



sudo firewall-cmd --list-all

sudo firewall-cmd --add-service=cockpit
sudo firewall-cmd --add-service=cockpit --permanent

sudo firewall-cmd --zone=public --permanent --add-service=http

sudo firewall-cmd --reload
```

---

## KVM & COCKPIT
>
[https://youtu.be/0I4MsIFw0uw](https://youtu.be/0I4MsIFw0uw) <br>
[https://computingforgeeks.com/how-to-install-kvm-on-fedora/](https://computingforgeeks.com/how-to-install-kvm-on-fedora/)<br>
[https://computingforgeeks.com/virsh-commands-cheatsheet/](https://computingforgeeks.com/virsh-commands-cheatsheet/)<br>




## RPM
```
rpm -ihv somepackage.rpm
```

---

## RDP
Default port: 3389
```
dnf install epel-release
dnf install xrdp
systemctl enable --now xrdp.service
systemctl enable --now xrdp-sesman.service
```




### ReadME
https://docs.fedoraproject.org/en-US/quick-docs/
https://docs.fedoraproject.org/en-US/fedora/f35/

