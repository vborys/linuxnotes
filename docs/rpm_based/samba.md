# SAMBA
* Listen ports smbd: 445, 139
* Listen ports nmbd: 137,138

---

## Install
```
dnf install samba samba-common samba-client {samba-winbind}
```

---

## Start/Enable
```
systemctl enable smb.service
systemctl start smb.service
systemctl status smb.service
```

```
systemctl enable nmb.service
systemctl start nmb.service
systemctl status nmb.service
```

---


## Firewall
```
firewall-cmd --zone=public --permanent --add-service=samba
firewall-cmd --reload
```

---

## Add accounts
```
groupadd smbgroup
useradd smbuser {1, 2, 3, ..}
usermod smbuser -aG smbgroup
smbpasswd -a smbuser
```

---


## Public share
```
mkdir -p /samba/share/public
chown -R nobody:nobody /samba/share/public
chmod -R 0775 /samba/share/public
```

---


## Private share
```
mkdir -p /samba/share/private
chmod -R 0770 /samba/share/private
chown -R root:smbgroup /samba/share/private
```

---


## SELinux config
```
sestatus
chcon -a -t samba_share_t /samba/share/public

(# semanage fcontext -a -t samba_share_t "/myshare(/.*)?")

setsebool -P samba_enable_home_dirs on
```

---

## Disable labels
```
restorecon -v /samba/share/public
```

---

## Connect to server
```
smbclient -U user //192.168.122.97/user
smbclient -L localhost -U user
```

---

## Mount SAMBA share
```
yum install cifs-utils -y
mkdir /mnt/sales
mount -t cifs -o user=user //192.168.0.119/salesdata /mnt/sales
```

---

### ReadME

https://www.howtoforge.com/how-to-install-samba-server-on-centos-8/ <br>
https://youtu.be/-zALd9F8r40