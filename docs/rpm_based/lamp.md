# L.A.M.P. stack

!!! quote "About"
        LAMP (Linux, Apache, MySQL, PHP/Perl/Python) is a very common example of a web service stack, named as an acronym of the names of its original four open-source components: the Linux operating system, the Apache HTTP Server, the MySQL relational database management system (RDBMS), and the PHP programming language.

        Source: [https://en.wikipedia.org/wiki/LAMP_(software_bundle)](https://en.wikipedia.org/wiki/LAMP_(software_bundle))

!!! success "Using"
        All configuring doing on CentOS 8.1


## Base Apache install
```
yum install httpd
systemctl start httpd
systemctl enable httpd

ss -tupln | grep httpd	# view port
```

---

**Firewalld allow**
```
firewall-cmd --list-all
firewall-cmd --add-service=http
firewall-cmd --add-service=http --permanent
```

---

## Config-test
```
/etc/httpd/conf/httpd.conf      # main cfg
/etc/httpd/conf.d/		        # adding cfg dir
/etc/httpd/conf.modules.d/      # mod dir
#apachectl configtest		# cfg check
 
/var/www & /var/www/html	# sites dir
```

**Modules**
```
/usr/lib64/httpd/modules	# mod dir
/etc/httpd/conf.modules.d	# conf mod dir
```

---

## Load module PHP 
```
yum install mod_php
systemctl restart httpd
```

Test index.php in /var/www/html/
```
---------- index.php -----------
<h1>HTTP Server - PHP page</h1>
<?php
	phpinfo(INFO_GENERAL)
?>
--------------------------------
```

---

## Load module WSGI (python mod)
```
yum install mod_wsgi
systemctl restart httpd
```

Test .py demo
```
#cp demo.py /var/www
#vi /etc/httpd/conf.d/wsgi.conf
```

```
---------wsgi.conf---------------
Scrip command   url alias   py-site file
WSGIScriptAlias /wsgidemo /var/www/demo.py
---------------------------------
```

---

## Virtual hosts
/usr/share/doc/httpd/httpdvhosts.conf	# sample conf
```
mkdir /var/www/http0{1,2}
echo '<h1>HTTP 01 Demo Site</h1>' > /var/www/http01/index.html
echo '<h1>HTTP 02 Demo Site</h1>' > /var/www/http02/index.html
cp /usr/share/doc/httpd/httpd-vhosts.conf /etc/httpd/conf.d/
vim /etc/httpd/conf.d/httpd-vhosts.conf
```

```
------------------- httpd-vhosts.conf ---------------------
<VirtualHost *:80>
    ServerAdmin webmaster@http01.local
    DocumentRoot "/var/www/http01"
    ServerName http01.local
    ServerAlias http01.local
    ErrorLog "/var/log/httpd/http01-error_log"
    CustomLog "/var/log/httpd/http01-access_log" common
</VirtualHost>
-----------------------------------------------------------
```


```
systemctl reload httpd
```

---

## Readme
Simple CentOS 8 Course by  Yuriy Lebedev [watch](https://www.youtube.com/watch?v=zSByTttfSnU&list=PLU4HoaX9cJ1DgCJJjToip2vVU7H18NTkc)

