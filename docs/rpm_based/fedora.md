# Fedora afterinstall

!!! quote "About"
        Fedora Linux is a Linux distribution developed by the community-supported Fedora Project which is sponsored primarily by Red Hat, a subsidiary of IBM, with additional support from other companies.

        Source: [https://getfedora.org/](https://getfedora.org/)


## Optimize DNF Config
```
sudo nano /etc/dnf/dnf.conf
fastestmirror=True
max_parallel_downloads=10
defaultyes=True
installonly_limit=2
```

---

## System Update
```
sudo dnf remove libreoffice-*
sudo dnf update
```

---

## Enable RPM
```
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```

---


## Installing Media Codecs
```
sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
sudo dnf install lame\* --exclude=lame-devel
sudo dnf group upgrade --with-optional Multimedia
```

---

## Install Extensions, Software, etc
```
sudo dnf install htop inxi gnome-tweaks gnome-extensions-app gnome-shell-extension-appindicator
```

---


## Install Programming Tools
```
sudo dnf install gcc-c++ pass mc
```

---

## Install fonts
```
sudo dnf install cabextract xorg-x11-font-utils 
sudo rpm -i https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm
```


```
wget https://assets.ubuntu.com/v1/fad7939b-ubuntu-font-family-0.83.zip 
unzip fad7939b-ubuntu-font-family-0.83.zip
sudo cp -rvf ubuntu-font-family-0.83 /usr/share/fonts/
sudo fc-cache -fv
```

---

## Remove old kernels
```
rpm -qa kernel\* |sort -V
dnf remove $(dnf repoquery --installonly --latest-limit=-2 -q)
```
>
CentOS, Red Hat (RHEL)
```
yum install yum-utils
package-cleanup --oldkernels --count=2
```

---



## Wayland Screen Recorder
```
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub io.github.seadve.Kooha
flatpak run io.github.seadve.Kooha
```

---

## Clean system
```
sudo dnf autoremove
sudo dnf clean all
```

---

### ReadME
https://youtu.be/-NwWE9YFFIg

https://docs.fedoraproject.org/en-US/quick-docs/setup_rpmfusion/

https://docs.fedoraproject.org/en-US/quick-docs/assembly_installing-plugins-for-playing-movies-and-music/