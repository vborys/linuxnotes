# Ubuntu afterinstall

!!! quote "About"
        Ubuntu is a Linux distribution based on Debian and composed mostly of free and open-source software.

        Source: [https://ubuntu.com/](https://ubuntu.com/)


## Update 

```
sudo apt update
sudo apt upgrade 
```

---


## Optimize SWAP

```
sudo nano /etc/sysctl.conf 
cat /proc/sys/vm/swappiness
reboot
```

---


## Install packages

```
#Ubuntu 20.04
sudo apt install ubuntu-restricted-extras gnome-tweaks htop screenfetch inxi tmux vim cmatrix gdebi gimp thonny libreoffice-base audacity gcompris-qt kolourpaint blender inkscape ktouch supertux supertuxkart mpv kdenlive breeze libre2-5 libfuse2 

#Ubuntu 22.04
sudo apt install ubuntu-restricted-extras gnome-tweaks htop screenfetch inxi tmux vim cmatrix gdebi gimp thonny libreoffice-base audacity gcompris-qt kolourpaint blender inkscape ktouch supertux supertuxkart mpv kdenlive breeze libre2-9 libfuse2 


#Ubuntu 24.04
sudo apt install ubuntu-restricted-extras gnome-tweaks htop screenfetch inxi tmux vim cmatrix gdebi gimp thonny libreoffice-base audacity gcompris-qt kolourpaint blender inkscape ktouch supertux supertuxkart mpv kdenlive breeze libre2-10 libfuse2t64 
```

---


## Additional Gnome config

```
cd /etc/xdg/autostart/
sudo sed --in-place 's/NoDisplay=true/NoDisplay=false/g' *.desktop

sudo ufw logging off

gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'
```

---


## Remove and disable SNAP 

```
snap list

#Ubuntu 20.04
sudo snap remove snap-store && sudo snap remove gtk-common-themes && sudo snap remove gnome-3-38-2004 && sudo snap remove core20 && sudo snap remove bare && sudo snap remove snapd && sudo apt purge snapd -y && sudo rm -rf /var/snap && sudo rm -rf /snap && sudo rm -rf /var/lib/snapd && rm -rf ~/snap


#Ubuntu 22.04
sudo snap remove --purge firefox && sudo snap remove --purge snapd-desktop-integration && sudo snap remove --purge snap-store && sudo snap remove --purge gtk-common-themes && sudo snap remove --purge gnome-42-2204 && sudo snap remove --purge core22 && sudo snap remove bare && sudo snap remove --purge snapd && sudo apt remove --autoremove snapd -y && sudo rm -rf /var/snap && sudo rm -rf /snap && sudo rm -rf /var/lib/snapd && rm -rf ~/snap


#Ubuntu 24.04
sudo snap remove --purge firefox && sudo snap remove --purge thunderbird && sudo snap remove --purge firmware-updater && sudo snap remove --purge snapd-desktop-integration && sudo snap remove --purge snap-store && sudo snap remove --purge gtk-common-themes && sudo snap remove --purge gnome-42-2204 && sudo snap remove --purge core22 && sudo snap remove bare && sudo snap remove --purge snapd && sudo apt remove --autoremove snapd -y && sudo rm -rf /var/snap && sudo rm -rf /snap && sudo rm -rf /var/lib/snapd && rm -rf ~/snap
```

```
sudo -H gedit /etc/apt/preferences.d/nosnap.pref

Package: snapd
Pin: release a=*
Pin-Priority: -10
```

---


## Disable autoupgrades

```
sudoedit /etc/apt/apt.conf.d/20auto-upgrades
```

>
APT::Periodic::Update-Package-Lists "0"; <br>
APT::Periodic::Download-Upgradeable-Packages "0"; <br>
APT::Periodic::AutocleanInterval "0"; <br>
APT::Periodic::Unattended-Upgrade "0";


```
sudo dpkg-reconfigure unattended-upgrades
```

---


## Remove old kernels

```
dpkg --list | egrep -i --color 'linux-image|linux-headers|linux-modules' | awk '{ print $2 }'

sudo apt purge linux-headers-5.11.0-27-generic linux-image-5.11.0-27-generic linux-modules-5.11.0-27-generic linux-modules-extra-5.11.0-27-generic

sudo apt remove unattended-upgrades && sudo apt update && sudo apt upgrade && sudo apt autoremove

reboot
```


## Install WPS Office

[How to localize WPS Office](https://github.com/wachin/wps-office-all-mui-win-language/blob/master/README.md)

```
#Ubuntu 20.04, 22.04
sudo cp -R uk_UA/ /opt/kingsoft/wps-office/office6/mui/
sudo cp -R dicts/uk_UA/ /opt/kingsoft/wps-office/office6/dicts/spellcheck/
sudo cp -R kstartpage/uk_UA/ /opt/kingsoft/wps-office/office6/addons/kstartpage/mui/
cp tmp/* ~/Шаблони/


#Ubuntu 24.04
cp -rf mui ~/.local/share/Kingsoft/office6
sudo cp /opt/kingsoft/wps-office/office6/mui/lang_list/lang_list_community.json /opt/kingsoft/wps-office/office6/mui/lang_list/lang_list_community.json.backup
sudo chmod -x /opt/kingsoft/wps-office/office6/wpscloudsvr
sudo mv /opt/kingsoft/wps-office/office6/wpscloudsvr /opt/kingsoft/wps-office/office6/wpscloudsvr.backup
sudo cp lang_list_community.json /opt/kingsoft/wps-office/office6/mui/lang_list/
sudo cp -R dicts/uk_UA/ /opt/kingsoft/wps-office/office6/dicts/spellcheck/
sudo cp -R kstartpage/uk_UA/ /opt/kingsoft/wps-office/office6/addons/kstartpage/mui/
cp tmp/* ~/Шаблони/
```


## Google Chrome

```
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo apt install -f


sudo nano /usr/share/applications/google-chrome.desktop
--password-store=basic
```


## Openboard 1.7

```
sudo apt install libminizip1 libre2-5 ./openboard_ubuntu_20.04_1.7.1_amd64.deb
```


## Gnome Extensions

```
sudo apt install gnome-shell-extension-manager gnome-shell-extension-draw-on-your-screen
```

[https://github.com/zhrexl/DrawOnYourScreen2](https://github.com/zhrexl/DrawOnYourScreen2)

```
#Ubuntu 22.04
mkdir -p ~/.local/share/gnome-shell/extensions/
git clone https://github.com/zhrexl/DrawOnYourScreen2 ~/.local/share/gnome-shell/extensions/draw-on-your-screen2@zhrexl.github.com
cd ~/.local/share/gnome-shell/extensions/draw-on-your-screen2@zhrexl.github.com/
git reset --hard 2615c7a
```



## QTile

[Install and config QTile (by Linux en Casa)](https://www.youtube.com/watch?v=fIiRSZXVNDo)

```
sudo apt install python3 python3-pip python3-venv python3-v-sim python-dbus-dev libpangocairo-1.0-0 python3-xcffib python3-cairocffi libxkbcommon-dev libxkbcommon-x11-dev kitty nitrogen fonts-font-awesome
mkdir -p ~/.local/src
cd ~/.local/src 
python3 -m venv qtile_venv
cd qtile_venv/
git clone https://github.com/qtile/qtile.git
bin/pip install qtile/.
bin/pip install psutil
cd bin/
sudo cp qtile /usr/local/bin/
cd /usr/share/xsessions/
sudo touch qtile.desktop
sudo nano qtile.desktop
```

>
[Desktop Entry] <br>
Name=Qtile <br>
Comment=Qtile Session <br>
Exec=qtile start <br>
Type=Application <br>
Keywords=wm;tiling <br>

```
cd /home/vborys/.config/qtile
chmod a+x autostart.sh

reboot
```




## Readme

https://easylinuxtipsproject.blogspot.com/p/speed-ubuntu.html
https://www.kevin-custer.com/blog/disabling-snaps-in-ubuntu-20-04/
https://linuxconfig.org/disable-automatic-updates-on-ubuntu-20-04-focal-fossa-linux
https://linuxhint.com/enable-disable-unattended-upgrades-ubuntu/
https://infoit.com.ua/linux/ubuntu/kak-ustanovit-drajver-amd-radeon-v-ubuntu-20-04-lts/
