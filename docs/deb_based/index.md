# About

![Image title](https://docs.boatswain.io/img/debian-ubuntu.png){ width=200, align=right }
This handbook is specially designed and compiled for beginers to systeming knowledge of this sphere.
I have given special priority to Linux commands, scripting, services and applications, access control, process control, user management - all you need to organize privat cybersecurity.

!!! info "Prerequisites:"
    All readers must have a little understanding of GNU/Linux basics and the passion to learn new technology.
