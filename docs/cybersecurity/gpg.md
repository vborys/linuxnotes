# GnuPG

!!! quote "About"
        GnuPG is a complete and free implementation of the OpenPGP standard as defined by RFC4880 (also known as PGP). GnuPG allows you to encrypt and sign your data and communications; it features a versatile key management system, along with access modules for all kinds of public key directories. GnuPG, also known as GPG, is a command line tool with features for easy integration with other applications. A wealth of frontend applications and libraries are available. GnuPG also provides support for S/MIME and Secure Shell (ssh). 

        Source: [gnupg.org](https://gnupg.org/)


## Install additional tools
```
sudo apt-get install rng-tools # Ubuntu command example
```

---

## Base commands
```
gpg --version
gpg --full-generate-key

~/.gnupg/gpg.conf               # config file
~/.gnupg/secring.gpg            # secret part of key
~/.gnupg/pubring.gpg            # private part of key

gpg -k	                        # list public keys 
gpg --list-keys --keyid-format LONG

gpg -K	                        # list privat keys
gpg --list-secret-keys --keyid-format LONG
```

---

## Edit key 
```
gpg --expert --edit-key $USERNAME

Sub menu commands:
> passwd       # change the passphrase
> clean        # compact any user ID that is no longer usable (e.g revoked or expired)
> revkey       # revoke a key
> addkey       # add a subkey to this key
> expire       # change the key expiration time
> adduid       # add additional names, comments, and email addresses
> addphoto     # add photo to key (must be JPG, 240x288 recommended, enter full path to image when prompted)

>save
```

---

## Advanced key operations
```
gpg -a --export-secret-key $USERNAME > secret_key	        # backup secret key
gpg -a --gen-revoke $USERNAME > revocation_cert.gpg	        # create revocaion certificate
gpg -a --export $USERNAME > public_key.gpg                  # export public key
gpg -a --export-secret-subkeys $USERNAME > secret_subs.gpg	# export secret subkeys
gpg --delete-secret-keys $USERNAME			        # delete secret key
gpg --import secret_subs.gpg				        # import secret subkeys
```

---

## Sign file & simple crypt
```
gpg --clearsign file.txt
gpg -r $USERNAME --armor --encrypt file.txt
gpg -d file.txt.asc
```

---

## Async crypt
```
gpg -o file.gpg -e -r $USERNAME file.txt        #encrypt
gpg -o file.txt -d file.gpg                     #decrypt

# operations with keyserver
gpg --keyserver pgp.mit.edu --send-key BB9A910B
gpg --keyserver pgp.mit.edu --search-keys v.v.borys@gmail.com
gpg --keyserver pgp.mit.edu --recv-keys BB9A910B
gpg --delete-secret-keys BB9A910B
gpg --delete-keys BB9A910B
gpg --keyserver pgp.mit.edu --refresh-keys
gpg --list-keys
```

## Readme
[https://wiki.archlinux.org/index.php/GnuPG](https://wiki.archlinux.org/index.php/GnuPG)

[https://youtu.be/I2mwqC6HGGE](https://youtu.be/I2mwqC6HGGE)
