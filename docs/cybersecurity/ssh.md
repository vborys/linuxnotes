# SSH (Secure Shell)

!!! quote "About"
        SSH is a software package that enables secure system administration and file transfers over insecure networks. It is used in nearly every data center and in every large enterprise.

        Source: [https://www.ssh.com/ssh/](https://www.ssh.com/ssh/)

## Basic setup

```
yum install openssh-server openssh-clients      # CentOS install command
cp /etc/ssh/sshd_config  /etc/ssh/sshd_config.original_copy
nano /etc/ssh/sshd_config

systemctl start sshd
systemctl enable sshd
```

---

## Connect to server by login

```
ssh user@192.168.0.120
```

---

## Connect to server by SSH-key

on CLIENT

```
.ssh/known_hosts	//SSH connection file
ssh-keygen -t rsa
chmod 700 ~/.ssh
chmod 600 ~/.ssh/id_rsa

ssh-copy-id user@192.168.0.120
//or on localhost 	
#cat ~/.ssh/id_rsa.pub >> ~/.ssh/autorized_keys

chmod 600 ~/.ssh/autorized_keys
```

on SERVER

```
nano /etc/ssh/sshd_config

        Port 22
        PermitRootLogin no
        PubkeyAuthentication yes
        PasswordAuthentication no
```

## Readme

Change ssh port: [youtube.com](https://youtu.be/s-zvkxy3Jo0?list=PLmZJWYVSxJ6xa63NTWRFa9WjDHcxej0wF)