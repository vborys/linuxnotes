# Cybersecurity & Linux

Basic security for everywhere


## Secure your web-browser

* How To Secure The Firefox Browser (by Distrowatch) [watch](https://youtu.be/1wz8U9vZe-s)
* How to configure Firefox settings for maximum privacy and security (by Hated One) [watch](https://youtu.be/tQhWdsFMc24)
* How to protect your online privacy in 2019 | Tutorial (by Hated One) [watch](https://youtu.be/lLessJ4R6w8)
