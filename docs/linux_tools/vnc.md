# TigerVNC Server 

!!! quote "About"
        Virtual Network Computing (VNC) is a graphical desktop-sharing system that uses the Remote Frame Buffer protocol (RFB) to remotely control another computer. 

        Source: [https://en.wikipedia.org/wiki/Virtual_Network_Computing](https://en.wikipedia.org/wiki/Virtual_Network_Computing)


## Install & run in Ubuntu 18.04

```
sudo apt install tigervnc-standalone-server tigervnc-common tigervnc-viewer
vncserver -kill :*
```

---

## Configure 

nano ~/.vnc/config
```
geometry=1920x1080
dpi=96
```

nano ~/.vnc/xstartup
```
#!/bin/sh
# Start Gnome 3 Desktop 
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
vncconfig -iconic &
dbus-launch --exit-with-session gnome-session &
```

chmod +x ~/.vnc/xstartup
```
vncserver :1 -localhost no -geometry 1024x768 -depth 32
vncserver -list
ss -tulpn | egrep -i 'vnc|590'
```

---

## Autorun service
**(must use nologin system enter)**

sudo nano /etc/systemd/system/vncserver@.service
```
[Unit]
Description=Start TightVNC server at startup
After=syslog.target network.target

[Service]
Type=forking
User=teacher
Group=teacher
WorkingDirectory=/home/teacher/

PIDFile=/home/teacher/.vnc/%H:%i.pid
ExecStartPre=-/usr/bin/vncserver -kill :%i > /dev/null 2>&1
ExecStart=/usr/bin/vncserver :%i -localhost no -geometry 1024x768 -depth 32
ExecStop=/usr/bin/vncserver -kill :%i

[Install]
WantedBy=multi-user.target
```
---

## Run vnc-process by systemd
```
sudo systemctl daemon-reload

sudo systemctl start vncserver@1.service
sudo systemctl status vncserver@1.service
sudo systemctl enable vncserver@1.service
```

## Readme

[https://www.tecmint.com/](https://www.tecmint.com/install-and-configure-vnc-server-on-ubuntu/)

[https://www.digitalocean.com/](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-vnc-on-ubuntu-18-04)

