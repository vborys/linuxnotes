# BASH-SCRIPTING

!!! quote "About"
        In addition to the interactive mode, where the user types one command at a time, with immediate execution and feedback, Bash (like many other shells) also has the ability to run an entire script of commands, known as a "Bash shell script".

        Source: [en.wikibooks.org](https://en.wikibooks.org/wiki/Bash_Shell_Scripting)

!!! success "Shell script order"
        1. Shebang
        2. Comments/file header
        3. Global variables
        4. Functions
                -use local variables
        5. Main script
        6. Exit with an exit status
                -exit <STATUS> at various exit point


## Start of bash script
```
#!/bin/bash
 or
#!/usr/bin/bash
```

---

## Run script
```
chmod a+x script.sh
$./script.sh

In BACH-scripts uses a same commands like in terminal
To run any CLI software use:
qterminal -e "htop"

sleep 3		//stop from 3 sec
```

---

## Comments
```
# single

:'
multi'
```

---

## Variables
```
# System vars
$USER 
$HOME 
$HOSTNAME 
$UID
$PS1		//cmd start
$LAN

# User vars 
name="User"
echo "$name" 	or 	echo ${name}

mydir=$(pwd)
echo "My word dir $mydir"

number1=10
number2=15
result=$(($number1+$number2))
echo "$result"
```

---

## IF statement
```
DIGITS				STRINGS
-eq (equal)	 	        	= 
-ge (greater or equal)		>= 
-le (less or equal) 		<= 
-gt (greater than)	        > 
-lt (less than) 	        <
-ne (not equal) 	        !=
		FOR VARS        
$num				str 

File operators (tests) [ some test operator  ]
-d FILE		//True if dir
-e FILE		//True if exist
-f FILE		//True if exist & regular file
-r FILE		//True if readable by you
-s FILE		//True if exis & not epmty
-w FILE		//True if writable for you
-x FILE 	//True if executable for you
-z STRING	//True if string is empty
-n STRING	//Treu if string is not empty
```

**Example1**
```
if [ a -eq 0  ]			//or	if(( a=0 ))
then echo ""
else echo "" (or elif [ ...  ])
fi
```

**Example2**
```
age=10
if [ "$age" -gt 18 ] && [ "age" -lt 40 ]
then
	echo "Age is correct"
else
	echo "Age in not correct"
fi
```

---

## FOR loop

**Example1**
```
for items in First Second Third
do
  echo "$items element"
done
```

**Example2**
```
COLORS="red green blue"
for COLOR in $COLORS
do 
  echo "COLOR: $COLORS"
done
```

**Example 3: Read file**
```
IFS=$"\n" 		// like python's end='\n'
file="file.txt"
for str in $(cat $file)
do
echo "$str"
done
```

**C-style FOR loop**
```
for ((i=1; i<=10; i++))
do
echo "$i"
done
```

---

## Positional parameters
```
$ script.sh param1 param2 param3

$0:"script.sh"
$1:"param1"
$2:"param2"
$3:"param3"

$@:"use all param"
```

---

## Userc input
```
read -p "PROMPT" VARIABLE		// read -p "Enter username" USER
```

---

## Check exit status
0       -success	

1       -error of execution

**Example1**
```
HOST="google.com"
ping -c 1 $HOST
if [ "$?" -eq "0"  ]
then
  echo "$HOST reachable"
else
  echo "$HOST unreachable"
fi
```

**Example2**
```
#!/bin/bash
HOST="google.com"
ping -c 1 $HOST
if [ "$?" -ne "0"  ]
then
  echo "$HOST unreachable"
  exit 1
fi
exit 0
```

---

## Functions
```
function function-name(){

}
or

function-name(){
	#code
}
```

**Example1**
```
#!/bin/bash
function hello(){
  echo "Hello $1"
}
hello Jason
```

**Example2**
```
#!/bin/bash
function hello(){
  for NAME in $@
  do 
   echo "Hello $1"
  done
}
hello Jason Dan Ryan
```

---

## Wildcards
for globing expand
```
*-matches zero or more characters
	*.txt
	a*
	a*.txt
?-matches one character
	?.txt
	a?
[]-character class
	[aeiou]
	ca[nt]*
	[!aeiou]*	/NOT
	[a-g]*		//range
	[3-6]*
```

**Named character classes**
```
[[:alpha:]]	//alphabetic
[[:alnum:]]	//alphabet & num
[[:digit:]]	//num
[[:lower:]]	
[[:space:]]	//space or tab
[[:upper:]]
```

**Example1**
```
for FILE in *.html
do
  cp $FILE /var/www
done
```

---

## CASE statement
```
case "$VAR" in 
  pattern_1)
	#Commands go here
	;;
  pattern_N)
	#Commands go here
	;;
esac
```

**Example1**
```
case "$1" in 
  start|START)
	/usr/sbin/sshd
	;;
  stop|STOP)
	kill $(cat /var/run/sshd.pid)
	;;
  *)
	echo "Usage: $0 start|stop" ; exit 1
	;;
esac
```

**Example2**
```
read -p "Enter y or n:" ANSWER
case "$ANSWER" in
  [yY] | [yY][eE][sS])
	echo "You answered yes"
	;;
  [nN] | [nN][oO])	
	echo "You answered no"
  *)
	echo "Invalid answer"
	;;
esac
```

---

## Logging
```
Syslog
/var/log/messages
/var/log/syslog

logger -p local0.info "Message"
```


---

## WHILE loop
```
while [ CONDITION IS TRUE ]
do
	command1
	command2
done
```

**Example 1**
```
INDEX=1
while [ $INDEX -lt 6  ]
do
 echo "Creating dir-${INDEX}"
 mkdir /home/project/dir-${INDEX}
 ((INDEX++))
done
```
**Example 2**
```
while [ "CORRECT" != "y"  ]
do
 read -p "Enter your name: " NAME
 read -p "Is ${NAME} correct? " CORRECT
done
```
**Example 3. Reading file**
```
LINE_NUM=1
while read LINE
do
 echo "${LINE_NUM}: ${LINE}"
 ((LINE_NUM++))
done < /etc/fstab
```
**Example 4. Reading with grep**
```
grep xfs /etc/fstab | while read LINE
do
 echo "xfs: ${LINE}"
done
```
**Example 5. Simple menu**
```
while true
do
 read -p "1:Show disk usage. 2:Show uptime." CHOISE
 case "$CHOISE" in
	1)
	 df -h
	 ;;
	2)
	 uptime
	 ;;
	*)
	 break
	 ;;
 esac
done
```

---


## Debuging scripts
```
#!/bin/bash -x 		//debuging all script lines ( -vx print lines of script before debuging)
TEST_VAR="test"
echo "$TEST_VAR"

output>>>
+TEST_VAR="test"
+echo "$TEST_VAR"
test


---


#!/bin/bash
...
set -x	//start debuging
...
set +x	//stop debuging
```

---

## Scripts example
```
#!/bin/bash
#Set BASH quit errors & quit scripts
set -e

#Functions
update(){
echo "Starting update..."
sudo apt update
sudo apt dist-upgrade -yy
}

clean (){
echo "Cleaning up..."
sudo apt autoremove -yy
sudo apt autoclean -yy
}
if [ "$1" == '--clean'  ]; then
	update
	clean
	exit
fi
```

**My GPG-script example**
```
#!/bin/bash
#
#var
filecr=''
filedecr=''
namecr=''
keyid=''
mail=''
echo "**********************************"
echo "***** Асиметричне шифрування *****"
echo "**** за допомогою PGP (GnuPG) ****"
echo "**********************************"
echo "Виберіть дію >>"
select opt in 'Генерувати_ключі' 'Відправити_на_публічний_сервер' 'Закодувати_файл' 'Розкодувати_файл' 'Вихід'
do
	if [ "$opt" == 'Генерувати_ключі' ]; then
		gpg --gen-key
		echo "Виконано!********** "
		gpg --list-keys
		./crypt.sh		
		break
	elif [ "$opt" == 'Відправити_на_публічний_сервер' ]; then
		echo "Введіть ваш e-mail"
		read mail
		echo "Введіть ID ключа"
		read keyid
		gpg --keyserver pgp.mit.edu --send-key $keyid
		echo "Виконано!********** "
		./crypt.sh
		break
	elif [ "$opt" == 'Закодувати_файл' ]; then
		echo "Введіть ім'я файлу для закодування"
		read filecr
		echo "Введіть ім'я власника ключа"
		read namecr
		gpg -o encrypt-file.gpg -e -r $namecr $filecr
		echo "Виконано!********** "
		./crypt.sh
		break
	elif [ "$opt" == 'Розкодувати_файл' ]; then
                echo "Введіть ім'я файлу для розкодування"
                read filedecr
                gpg -o decrypt-file -d $filedecr
                echo "Виконано!********** "
                ./crypt.sh
                break
	elif [ "$opt" == 'Вихід' ]; then
		break
	fi
done
```


## Readme
```
https://youtu.be/PpmyVXCdiDY?list=PL7KBbsb4oaOnz1FOlCDof2oSgEE-YDywp
https://www.youtube.com/watch?v=e7BufAVwDiM
https://youtu.be/RHUfIXgHbzw
```