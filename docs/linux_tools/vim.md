# VIM editor

!!! quote "About"
        "Vim is a highly configurable text editor built to make creating and changing any kind of text very efficient. It is included as "vi" with most UNIX systems and with Apple OS X"  
        Source: [https://www.vim.org/](https://www.vim.org/)

---

## Configuring VIM

~/.vimrc

For example:
```
set number
syntax enable
```
---

## Base commands

```
:e hello.txt	# open (edit) file
:r		        # read another file (appending to current)
:w hello.txt	# write to file
:q		        # quit editor
:q!		        # quit without save
ZZ              # :wq
ZQ              # :q!
```

---

## Editor modes

```
i		# insert mode
A		# append to end insert mode
ESC		# command mode
v		# visual mode
```

---

## Text navigate

```
h       # arrow <
l       # arrow >
k       # arrow ^
j       # arrow v

gg      # begin file
G       # end file

x       # delete char
u       # undo change
```

---

## Operations with a text

```
y	# copy (copy) selection
yy	# yank (copy) line	
yw	# yank (copy) word
yl	# yank (copy) letter

dd	# delete line
dw	# delete word (cursor must be on first letter of word)
dl	# delete letter

p	# paste all
```

---

## Additional functions

```
/text                   # search text(^ to history)
:sort ui                # sort line a->z in visual mod
:%s/word1/word2/g       # find & replace word1 to word2 
:vsplit textfile2.txt   # split window with 2 opened files (Ctrl+ww to switch)
Ctrl+v  (select area to comment) Shift+i #(or other symbol) Esc 	# comment many rows
:.!ls                   # copy terminal output to doc
```