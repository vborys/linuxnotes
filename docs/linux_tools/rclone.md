# Rclone 

!!! quote "About"
        Rclone is a command-line program to manage files on cloud storage. 
        It is a feature-rich alternative to cloud vendors' web storage interfaces. 
        Over 70 cloud storage products support rclone including S3 object stores, 
        business & consumer file storage services, as well as standard transfer protocols. 

        Source: [https://rclone.org/](https://rclone.org/)

## Install

### Rclone Utility

[https://rclone.org/downloads/](https://rclone.org/downloads/)

### Rclone Browser

[https://github.com/kapitainsky/RcloneBrowser](https://github.com/kapitainsky/RcloneBrowser)



## Sync Google Photos

The rclone backend for Google Photos is a specialized backend for transferring photos and videos to and from Google Photos.

### Configuration
The initial setup for google cloud storage involves getting a token from Google Photos which you need to do in your browser. rclone config walks you through it.

**Check for updates: [https://rclone.org/googlephotos/](https://rclone.org/googlephotos/)**

```
No remotes found, make a new one?
n) New remote
s) Set configuration password
q) Quit config
n/s/q> n
name> remote
Type of storage to configure.
Enter a string value. Press Enter for the default ("").
Choose a number from below, or type in your own value
[snip]
XX / Google Photos
   \ "google photos"
[snip]
Storage> google photos
** See help for google photos backend at: https://rclone.org/googlephotos/ **

Google Application Client Id
Leave blank normally.
Enter a string value. Press Enter for the default ("").
client_id> 
Google Application Client Secret
Leave blank normally.
Enter a string value. Press Enter for the default ("").
client_secret> 
Set to make the Google Photos backend read only.

If you choose read only then rclone will only request read only access
to your photos, otherwise rclone will request full access.
Enter a boolean value (true or false). Press Enter for the default ("false").
read_only> 
Edit advanced config? (y/n)
y) Yes
n) No
y/n> n
Remote config
Use web browser to automatically authenticate rclone with remote?
 * Say Y if the machine running rclone has a web browser you can use
 * Say N if running rclone on a (remote) machine without web browser access
If not sure try Y. If Y failed, try N.
y) Yes
n) No
y/n> y
If your browser doesn't open automatically go to the following link: http://127.0.0.1:53682/auth
Log in and authorize rclone for access
Waiting for code...
Got code

*** IMPORTANT: All media items uploaded to Google Photos with rclone
*** are stored in full resolution at original quality.  These uploads
*** will count towards storage in your Google Account.

Configuration complete.
Options:
- type: google photos
- token: {"access_token":"XXX","token_type":"Bearer","refresh_token":"XXX","expiry":"2019-06-28T17:38:04.644930156+01:00"}
Keep this "remote" remote?
y) Yes this is OK
e) Edit this remote
d) Delete this remote
y/e/d> y
```

## Info

[Sync Google DRIVE in Linux Using Rclone (by STÆMPUNK TV)](https://www.youtube.com/watch?v=ff8Ogk8NIPU)

