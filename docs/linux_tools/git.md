# GIT (by using GitLab)

!!! quote "About"
        Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. 

        Source: [https://git-scm.com/](https://git-scm.com/)

## Configure
```
git --version
git init				        # initialize local project
git config --global user.name "vborys"
git config --global user.email "vborys@tuta.io"

git config --global --list 		# list all config settings
```

---

## Additional settings
```
git config --global gpg.program gpg2
git config --global user.signingkey ***
git config --global commit.gpgsign true
```

---

## Add files
```
git add {filename}		# add file or apply file changes
git status
```

---

## Remove files
```
git rm --cached {filename}	# remove file from project
```

---

## Commit
```
git commit			# initial commit
git commit -m "My commit"
```

---

## Add .gitignore
```
touch .gitingore		# file with list of ignore files or dirs

.gitignore
somefile.txt
/somedir

```

---

## Send project to remove repository
```
git remote add origin https://github.com/username/project.git
git push -u origin master
git push	# push init project
```

---

## Pull
``` 
git pull 	# pull latest changes from remote repository
```

---

## Clonning
```
git clone https://github.com/username/project.git
```

---

## Open project by editor
```
code .		# open GitProject by VScode (or Atom)
```

---

## Connect to git by ssh-key
* Generate ssh-key
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"		# generating a new SSH key
eval "$(ssh-agent -s)"			# adding your SSH key to the ssh-agent
ssh-add ~/.ssh/id_rsa
---OR only--
ssh-keygen
```
* Adding a new SSH key to your GitHub account

In the user settings sidebar, click SSH and GPG keys.

---

## List of repos
```
git remote
```

---

## Create new branch
```
git branch new_branch origin/new_branch
```

---

## Switch to other branch
```
git checkout new_branch
```

---

## List local branches
```
git branch
```

---

## List all branches
```
git branches -a
```

---

## Merge with origin branch (after switching)
```
git merge new_branch
```

---

## Delete branch
```
git branch -D new_branch
```

---

## Loging
```
git log
```

---

## Reset (after hashing)
```
git reset --hard a3775a5485af0af20375cedf46112db5f813322a 
git push --force
```

---

## Readme 

* GitLab Beginner Tutorial [watch](https://youtu.be/Jt4Z1vwtXT0?list=PLhW3qG5bs-L8YSnCiyQ-jD8XfHC2W1NL_)
* Git & GitHub Crash Course For Beginners [watch](https://youtu.be/SWYqp7iY_Tc)
* Host a static website for FREE on GitHub [watch](https://youtu.be/M5mg0r4ajt4)
* Using GitLab Pages [watch](https://www.youtube.com/watch?v=xI0FPmYIXIE)

**MKDocs in GitLab**

* MKDocs in Gitlab Pages. [Part 1](https://youtu.be/k7rkjVfuB2M)
* MKDocs in Gitlab Pages. [Part 2](https://youtu.be/oDAHnwmPfjA)
* User guide for MkDocs [https://www.mkdocs.org/](https://www.mkdocs.org/)
* Technical documentation | [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)
