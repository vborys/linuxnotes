# About

**Welcome to “Linux Administration Handbook” by [vborys](https://vborys.gitlab.io).**

![Image title](img/logo1.png){ width=85, align=right }
This handbook is specially designed and compiled for beginers to systeming knowledge of this sphere.
I have given special priority to Linux commands, scripting, services and applications, access control, process control, user management - all you need to organize privat cybersecurity.

!!! warning "Prerequisites:"
    All readers must have a little understanding of GNU/Linux basics and the passion to learn new technology.

!!! success "Distributions:"
    This guide is presently supported by the latest releases of Linux distributions like CentOS, Fedora, Debian, Ubuntu, etc. 
